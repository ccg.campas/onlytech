import React from 'react';
import {Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'jquery/dist/jquery';

//hide show based on signed in, investor/developer/entrepenuer

export default function Navigation(){
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <div className="container-fluid">
                <div className="navbar-header">
                    <a href="/" className="navbar-brand">otaku project</a>
                </div>
                    <button data-target="#main-menu" data-toggle="collapse" className="navbar-toggler">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div id="main-menu" className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item"><Link to="/" className="nav-link">DASHBOARD</Link></li>
                            <li className="nav-item"><Link to="/profile" className="nav-link">PROFILE</Link></li>
                            <li className="nav-item"><Link to="/createNew" className="nav-link">CREATE NEW PROJECT</Link></li>
                            <li className="nav-item"><Link to="/logIn" className="nav-link">LOG IN</Link></li>
                        </ul>
                    </div>       
            </div>
        </nav>
    );
}
